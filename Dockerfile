From python:3.10

COPY . /opt
WORKDIR /opt
RUN pip install -r requirements.txt

CMD [ "python", "/opt/app/app.py" ]
