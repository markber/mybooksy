#*****************************************************************************
#*****************************************************************************
#from crypt import methods
from crypt import methods
from email import message
import email
from pickle import TRUE
from flask import Flask, request, flash, url_for, redirect, render_template, session
from flask_login import login_user, logout_user, login_required
from werkzeug.security import generate_password_hash, check_password_hash
from flask_sqlalchemy import SQLAlchemy
import datetime
import uuid
from datetime import datetime, timedelta
import random
import string
import qrcode
import base64
from configparser import ConfigParser
import json
import glob
from hashlib import sha256
import os


#*****************************************************************************
#*****************************************************************************

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///user.sqlite3'
app.secret_key = 'ItShouldBeAnythingButSecret' 
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

class NewUser(db.Model):
   id = db.Column('userid', db.Integer, autoincrement=True, primary_key = True)
   lname = db.Column(db.String(50))
   fname = db.Column(db.String(50))
   email = db.Column(db.String(50)) 
   pwd = db.Column(db.String(100))
   agb = db.Column(db.String(50))
   xcode = db.Column(db.String(50))
   xdate = db.Column(db.String(50))
   xtime = db.Column(db.String(50))
   xregister = db.Column(db.String(2))
   profil = db.Column(db.String(2))

def __init__(self, fname, lname, email, pwd, agb, xcode, xdate, xtime, xregister, profil):
   self.fname = lname
   self.lname = fname
   self.email = email
   self.pwd = pwd
   self.agb = agb
   self.xcode = xcode
   self.xdate = xdate
   self.xtime = xtime
   self.xregister = xregister
   self.profil = profil 

class Desknr(db.Model):
    idnr = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    desk = db.Column(db.String(100))
    filename = db.Column(db.String(100))
    booking_email = db.Column(db.String(100))
    booking_code = db.Column(db.String(100))
    booking_stat = db.Column(db.String(1))

def __init__(self, desk, filename, booking_email, booking_code, booking_stat):
    self.desk = desk
    self.filename = filename
    self.booking_email = booking_email
    self.booking_code = booking_code
    self.booking_stat = booking_stat

class Mydesk(db.Model):
    deskid = db.Column(db.Integer, primary_key=True) # primary keys are required by SQLAlchemy
    userid = db.Column(db.String(100))
    booking_date = db.Column(db.String(100))
    validto = db.Column(db.String(100))
    booked = db.Column(db.String(100)) 
    bookingcode = db.Column(db.String(100)) 
    desk = db.Column(db.String(100))

def __init__(self, userid, booking_date, validto, booked, bookingcode,desk):
    self.userid = userid
    self.booking_date = booking_date
    self.validto = validto
    self.booked = booked
    self.bookingcode = bookingcode
    self.desk = desk

####################### END DATABASE #############################
##################################################################
####################### SHORT POINT ##############################

path = r"static/*.png"
@app.route('/t')
def t():
    page_title = {'name':'Book a Desk...'}
    # cont free desks!
    #xcount = Desknr.query.filter_by(booking_email='0', booking_code="0").count()
    #bookeddesk = Desknr.query.filter(Desknr.booking_email!='0')

    #fnames = [x.name for x in os.scandir(path)]
    
    xresult =  IMG.query.order_by(IMG.filename.asc()).all()

    return render_template('t.html', page_title=page_title,files=xresult)
####################### SHORT QR BOOKING #########################

####################### END DATABASE #############################
##################################################################
####################### SHORT POINT ##############################
@app.route('/')
def index():
    page_title = {'name':'Book a Desk...'}
    # cont free desks!
    xcount = Desknr.query.filter_by(booking_email='0', booking_code="0").count()
 
    xresult_booked =  Desknr.query.filter_by(booking_stat="1").all()
    
    xresult =  Desknr.query.order_by(Desknr.filename.asc()).all()

    return render_template('index.html', page_title=page_title, xcount=xcount, files=xresult, xresult_booked=xresult_booked )
####################### SHORT QR BOOKING #########################
@app.route('/shortbooking', methods = ['GET', 'POST'])
def shortbooking():
    page_title = {'name':'Book a Desk, short QR code Booking...'}
    
    if not 'deskme' in request.args:
        message = "something goes wrong (no valid GET), please try again or login!"
        flash(message)
        return redirect(url_for('error')) 
    else:
        deskme = request.args['deskme']

        a = deskme.split("?validation=")
        print("+++++++++++++++++++++++++++++")
        a = a[0]
      
        base64_bytes = a.encode('ascii')
        message_bytes = base64.b64decode(base64_bytes)
        base64_message = message_bytes.decode('ascii')
        print(base64_message)

        y = json.loads(base64_message)

        deskme = y["desknr"]
        isbooking = y["booking"]

        xisbooking = Desknr.query.filter_by(desk=isbooking).first()
        if xisbooking:
            xme = xisbooking.booking_email
            xisbooking_user = NewUser.query.filter_by(email=xme).first()
            if xisbooking_user:
                userid= xisbooking_user.id
                fname= xisbooking_user.fname
                lname = xisbooking_user.lname
                xbookingtime = Mydesk.query.filter_by(userid=userid).first()
                if xbookingtime:
                    bookeduntil = xbookingtime.validto
                    return render_template('see.html', page_title=page_title, deskme=deskme, email=xme,fname=fname, lname=lname, bookeduntil=bookeduntil)


        xdesknr = Desknr.query.filter_by(desk=deskme).first()
        if not xdesknr:
            message = "something goes wrong (no valid desk), please try again or login!"
            flash(message)
            return redirect(url_for('error'))   
        if request.method == 'POST':
            email=request.form['email']
            xvalid = request.form['validto']        
            if not email:
                flash('email is missing or wrong, please try again or login!')
                return redirect(url_for('error'))            
            else:
                userid_qr = NewUser.query.filter_by(email=email).all()
                for xid in userid_qr:
                    xuserid= xid.id
            user = NewUser.query.filter_by(email=email).first()
            if not user:
                message = "email address ( {} ) is not registerd, you need first register a account".format(email)
                flash(message)
                return redirect(url_for('error'))  

            checkbooking = Mydesk.query.filter_by(userid=xuserid).all()
            if checkbooking:
                message = "you ( {} ) have already booked a desk!, please enter booking code or login!".format(email)
                flash(message)
                return redirect(url_for('deleteqr'))
            xdesk = Desknr.query.filter_by(booking_email=email).first()
            if xdesk:
                return redirect(url_for('deleteqr'))
    
            if xvalid == "1":
                xxtimestamp = datetime.now()
                xxdate=xxtimestamp.strftime("%d.%m.%Y %H:%M:%S")
                validto = datetime.now() + timedelta(hours=8)
                validto = validto.strftime("%d.%m.%Y %H:%M:%S")
            else:
                xxtimestamp = datetime.now()
                xxdate=xxtimestamp.strftime("%d.%m.%Y %H:%M:%S")
                validto = datetime.now() + timedelta(hours=4) 
                validto = validto.strftime("%d.%m.%Y %H:%M:%S")

            booking_code_ln = 8
            booking_code = ''.join(random.choices(string.ascii_letters+string.digits,k=booking_code_ln))  

            xbook = Mydesk(userid=xuserid,booking_date=xxdate, validto=validto, booked='booked',bookingcode=booking_code,desk=deskme)
            db.session.add(xbook)
            db.session.commit()

            Desknr.query.filter_by(desk=deskme).update(dict(booking_email=email, booking_code=booking_code, booking_stat="1"))
            db.session.commit()
            #return redirect(url_for('info', desk=deskme))    
            return render_template('info.html',page_title=page_title,desk=deskme)
            
    return render_template('shortbooking.html', page_title=page_title, deskme=deskme)
#####################################################################
####################### START REGUSTER ##############################
#####################################################################
@app.route('/register', methods = ['GET', 'POST'])
def register():
    page_title = {'name':'Book a Desk, Registration...'}
    xcount = Desknr.query.filter_by(booking_email='0', booking_code="0").count()
    #date and time
    xtimestamp = datetime.now()
    xdate=xtimestamp.strftime("%d.%m.%Y")
    xtime=xtimestamp.strftime("%H:%M:%S")

    #UUID for registratio-code
    xcode=str(uuid.uuid4())

    #default value registration 0=disabled, 1=vali
    xregister = 0

    if request.method == 'POST':
        if not request.form['lname'] \
            or not request.form['fname']\
                or not request.form['email']\
                    or not request.form['pwd1']\
                        or not request.form['pwd2']\
                            or not request.form['agb']:
            
            flash('Please enter all the fields', 'error')
            return redirect(url_for('register')) 
        else:
            newuser = NewUser(lname=request.form['lname'],\
                fname=request.form['fname'],\
                    email=request.form['email'],\
                            pwd=generate_password_hash(request.form['pwd2'], method='sha256'),\
                                    agb=request.form['agb'],\
                                        xcode=xcode,\
                                            xdate=xdate,\
                                                xtime=xtime,\
                                                    xregister=xregister,\
                                                        profil="0")

            xpwd1 = request.form['pwd1']
            xpwd2 = request.form['pwd2']
            if xpwd1 != xpwd2:
                flash('Passwords do not match', 'error')
                return redirect(url_for('register')) 

            user = NewUser.query.filter_by(email=request.form['email']).first() # if this returns a user, then the email already exists in database
            if user:
                flash('Email address already exists.')
                return redirect(url_for('register'))   
            else:                     
                db.session.add(newuser)
                db.session.commit() 
                return redirect(url_for('login'))   
    return render_template('register.html', page_title=page_title, xcount=xcount)
#########################################################################
####################### END REGUSTER ####################################
####################### START LOGIN #####################################
#########################################################################
@app.route('/login', methods = ['GET', 'POST'])
def login():

    page_title = {'name':'Book a Desk, Login...'}
    xcount = Desknr.query.filter_by(booking_email='0', booking_code="0").count()
    if request.method == 'POST':
        pwd1=request.form['pwd']
        email=request.form['email']
        
        xlogin = NewUser.query.filter_by(email=email).all()

        if not xlogin:
            message = "the email address ( {} ) is not registered yet, please register a new account first".format(email)  # Using the format method
            flash(message)
            return redirect(url_for('login'))           
        
        for row in xlogin:
            id = row.id
            email = row.email
            fname = row.fname
            lname = row.lname
            pwd = row.pwd
            profil = row.profil

            if profil == "500":
                profil="admin"
            else:
                profil="user"
            
            verifyPWD=check_password_hash(pwd, pwd1)
            if verifyPWD:
                session['id'] = id
                session['email'] = email
                session['fname'] = fname
                session['lname'] = lname
                session['logged_in'] = True
                session['profil'] = profil
                return redirect(url_for('dashboard', email=email, profil=profil))
            else:
                flash('something goes wrong, please login')
                return redirect(url_for('login'))

    return render_template('login.html', page_title=page_title, xcount=xcount)
#########################################################################
####################### END LOGIN #######################################
#########################################################################
####################### START DASHBOARD #################################
#########################################################################
@app.route('/dashboard', methods = ['GET', 'POST'])
def dashboard():

    if 'email' in session:
        username = session['fname']
        id = session['id']
    else:
        flash('something goes wrong, please login')
        return redirect(url_for('login'))

    page_title = {'name':'Book a Desk, Login...'}
    xcount = Desknr.query.filter_by(booking_email='0', booking_code="0").count()
    if 'email' in session:
        username = session['fname']
        email = session['email']
        id = session['id']
        profil = session['profil']

        xdesk = Desknr.query.filter_by(booking_email=email).first()
        if xdesk:
            return redirect(url_for('delete'))

        Mydesk.query.filter_by(userid=id).delete()
        xdesk = Desknr.query.filter_by(booking_email='0', booking_code="0").order_by(Desknr.desk.asc()).all()

        if request.method == 'POST':
            desk1 = request.form.get('desk')
            xvalid = request.form.get('validto')

            if xvalid == "1":
                xxtimestamp = datetime.now()
                xxdate=xxtimestamp.strftime("%d.%m.%Y %H:%M:%S")
                validto = datetime.now() + timedelta(hours=8)
                validto = validto.strftime("%d.%m.%Y %H:%M:%S")
            else:
                xxtimestamp = datetime.now()
                xxdate=xxtimestamp.strftime("%d.%m.%Y %H:%M:%S")
                validto = datetime.now() + timedelta(hours=4) 
                validto = validto.strftime("%d.%m.%Y %H:%M:%S")
            
            booking_code_ln = 8
            booking_code = ''.join(random.choices(string.ascii_letters+string.digits,k=booking_code_ln))  
            session['booking_code'] = booking_code

            xbook = Mydesk(userid=id,booking_date=xxdate, validto=validto, booked='booked',bookingcode=booking_code,desk=desk1)
            db.session.add(xbook)
            db.session.commit()

            Desknr.query.filter_by(desk=desk1).update(dict(booking_email=email, booking_code=booking_code, booking_stat="1"))
            db.session.commit()

            return redirect(url_for('booked'))

        return render_template('profile.html', page_title=page_title, username=username,desknr=xdesk, xcount=xcount, profil=profil)
#########################################################################
####################### START INFO ######################################
@app.route('/info')
def info():
    page_title = {'name':'Book a Desk, details...'}
    return render_template('info.html', page_title=page_title)
#########################################################################
####################### END INFO ########################################
####################### ADMIN ###########################################
@app.route('/admin')
def admin():
    page_title = {'name':'mybooksy, admin panel...'}

    return render_template('admin.html', page_title=page_title)


####################### SHORT SHOW-ALL ##################################
#########################################################################
@app.route('/showbookings')
def showbookings():
    page_title = {'name':'Book a Desk, short show bookings...'}
    xcount = Desknr.query.filter_by(booking_email='0', booking_code="0").count()
    
    if 'id' in session:
        username = session['fname']
        id = session['id']
        profil = session['profil']

    else:
        flash('something goes wrong, please login')
        return redirect(url_for('login'))
        
    userdetails = NewUser.query.filter_by(id=id).all()
    allbooking = Mydesk.query.filter_by(userid=id)


    return render_template('allbookings.html', page_title=page_title, allbooking=allbooking,xcount=xcount,username=username,userdetails=userdetails,profil=profil)
####################### SHOW USERS ALL ##################################
#########################################################################
@app.route('/showusers')  
def showusers():
    page_title = {'name':'Book a Desk, show users...'}
    xcount = Desknr.query.filter_by(booking_email='0', booking_code="0").count()
   
    if 'email' in session:
        username = session['fname']
        id = session['id']
        email = session['email']
        profil = session['profil']
      
        my_users = NewUser.query.filter_by(id=id).all()
    else:
        flash('something goes wrong, please login')
        return redirect(url_for('login'))

    return render_template('showusers.html',email=email, page_title=page_title, my_users=my_users,xcount=xcount,username=username, profil=profil)
####################### BOOKING #########################################
#########################################################################
####################### EDIT USER  ##################################
#########################################################################
@app.route('/edit', methods=['POST', 'GET'])
def edit():
    page_title = {'name':'Book a Desk, show users...'}
    xcount = Desknr.query.filter_by(booking_email='0', booking_code="0").count()
   
    if not 'email' in session:
        flash('something goes wrong, please login')
        return redirect(url_for('login')) 
    
    username = session['fname']
    id = session['id']
    profil = session['profil']

    if request.method == 'POST':
        fname=request.form['fname']
        lname=request.form['lname']
        xemail=request.form['email']
        print( fname, lname, xemail)

        NewUser.query.filter_by(id=id).update({'lname':lname, 'fname':fname, 'email':xemail})
        db.session.commit()

        message = "successful change new  ( {}, {}, {})!".format(fname, lname, xemail)
        flash(message)
        return redirect(url_for('showusers')) 
        
      
    my_users = NewUser.query.filter_by(id=id).all()

    return render_template('edit.html', page_title=page_title, my_users=my_users,xcount=xcount,username=username, profil=profil)
####################### BOOKING #########################################
#########################################################################
@app.route('/update', methods=['POST', 'GET'])
def update():
    
    if request.method == 'POST':
        fname=request.form['fname']
        lname=request.form['lname']
        xemail=request.form['email']
        print( fname, lname, xemail)

########################################################################
@app.route('/booked')
def booked():
    page_title = {'name':'Book a Desk, details...'}
    if 'email' in session:
        username = session['fname']
        id = session['id']
        
        my_booking = Mydesk.query.filter_by(userid=id).all()
    else:
        flash('something goes wrong, please login')
        return redirect(url_for('login'))

    return render_template('booking.html', page_title=page_title, username=username, my_booking=my_booking)
####################### BOOKING QR DEL ##################################
#########################################################################
@app.route('/deleteqr', methods = ['GET', 'POST'])
def deleteqr():
    page_title = {'name':'Book a Desk, remove QR booking...'}
    if request.method == 'POST':
        qr=request.form['qr']
        
        if not qr:
            flash('please enter a valid booking code')
            return redirect(url_for('deleteqr'))  

        xvalidcode = Desknr.query.filter_by(booking_code=qr).first()
       
        if xvalidcode:
            Mydesk.query.filter_by(bookingcode=qr).delete()
            Desknr.query.filter_by(booking_code=qr).update(dict(booking_email='0', booking_code="0", booking_stat="0"))
            db.session.commit()
            message = "booking code ( {} ) was successfully removed!".format(qr)
            flash(message)
            return redirect(url_for('shortbooking')) 
        else:
            message = "booking code ( {} ) was no valid!".format(qr)
            flash(message)
            return redirect(url_for('shortbooking')) 

    return render_template('removeqr.html', page_title=page_title)
####################### END BOOKING QR DEL ##############################
#########################################################################

#########################################################################

@app.route('/deletedesk', methods = ['GET', 'POST'])
def deletedesk():
    if 'desk_code' in request.args:
        deldesk = request.args['desk_code']
        print("-----------------" + deldesk) 
        xdeldesk = Desknr.query.filter_by(desk=deldesk).first()
        print(xdeldesk)
        if xdeldesk:
            Desknr.query.filter_by(desk=deldesk).delete()
            db.session.commit()
            os.remove("app/static/" + deldesk + ".png")


    return redirect(url_for('genqr'))
 
####################### END BOOKING QR DEL ##############################
####################### DEL USER  #######################################
#########################################################################
@app.route('/deleteuser', methods = ['GET', 'POST'])
def deleteuser():

    if 'register_code' in request.args:
        deleteuser = request.args['register_code']
        print(deleteuser)
        xdeleteuser = NewUser.query.filter_by(xcode=deleteuser).first()

        if xdeleteuser:
            NewUser.query.filter_by(xcode=deleteuser).delete()
            db.session.commit()

        else:
            flash('booking code is not valid!')
            return redirect(url_for('error'))  



    return redirect(url_for('showusers'))
####################### END DEL USER  ###################################
#########################################################################
@app.route('/deleteqr_get', methods = ['GET', 'POST'])
def deleteqr_get():
    page_title = {'name':'Book a Desk, delete QR-code Booking...'}

    if 'booking_code' in request.args:
        booking_code = request.args['booking_code']
        print(booking_code)
        xbookingcode = Desknr.query.filter_by(booking_code=booking_code).first()

        if xbookingcode:
            Mydesk.query.filter_by(bookingcode=booking_code).delete()
            Desknr.query.filter_by(booking_code=booking_code).update(dict(booking_email='0', booking_code="0", booking_stat="0"))
            db.session.commit()

        else:
            flash('booking code is not valid!')
            return redirect(url_for('error'))  

    else:
        return redirect(url_for('error'))

    return redirect(url_for('showbookings'))
#########################################################################
#########################################################################
@app.route('/genqr', methods = ['GET', 'POST'])
def genqr():
    page_title = {'name':'Book a Desk, QR-code Booking...'}
    xcount = Desknr.query.filter_by(booking_email='0', booking_code="0").count()

    if not 'email' in session:
        flash('something goes wrong, please login')
        return redirect(url_for('login'))
    
    profil = session['profil']
    fname = session['fname']
    config = ConfigParser()
    config.read('app/base/config.cfg')
    
    protocol = config['MyServer']['protocol']
    domain = config['MyServer']['domain']
    port = config['MyServer']['port']
    site = config['MyServer']['site']
    get1 = config['MyServer']['get1']
    get2 = config['MyServer']['get2']
    validation_key = config['MyServer']['key']
    validation_key = sha256(validation_key.encode('utf-8')).hexdigest()

    if request.method == 'POST':
        qrcode1=request.form['qrcode1']

        qr = qrcode.QRCode(version = 1,box_size = 15,border = 10)
        message_get = "{}\"{}\":\"{}\",\"{}\":\"{}\"{}".format("{",get1,qrcode1,get2,qrcode1,"}")
        print(message_get)
        message_bytes = message_get.encode('ascii')
        base64_bytes = base64.b64encode(message_bytes)
        base64_message = base64_bytes.decode('ascii')

        message_url = "{}://{}:{}/{}?deskme={}?validation={}".format(protocol,domain,port,site,base64_message,validation_key)
        print(message_url)
        
        print(base64_message)
        data = message_url
        qr.add_data(data)
        qr.make(fit=True)
        img = qr.make_image(fill_color = 'black', back_color = 'white')
        qrfile = "{}.png".format(qrcode1)
        img.save("app/static/" + qrfile)
        print("app/static/" + qrfile)

        checkifbooked = Desknr.query.filter_by(desk=qrcode1, booking_stat="1").all()
        if checkifbooked:
            message = "desk {} is still booked, you cannot replace qr-code, please remove first booking".format(qrcode1)
            flash(message)
            return redirect(url_for('genqr')) 
        else:
            Desknr.query.filter_by(filename=qrfile).delete()
            db.session.commit()
            new_file = Desknr(desk=qrcode1,filename=qrfile, booking_email="0", booking_code="0", booking_stat="0")
            db.session.add(new_file)
            db.session.commit() 
    else:
        print("help")
           
    xresult =  Desknr.query.order_by(Desknr.filename.asc()).all()
    ifbooked = Desknr.query.filter_by(booking_stat="1").all()

    return render_template('genqrcode.html', xcount=xcount, page_title=page_title, username=fname, files=xresult, ifbooked=ifbooked, profil=profil)



@app.route('/delete')
def delete():
    page_title = {'name':'Book a Desk, Login...'}
    if 'email' in session:
        username = session['fname']
        id = session['id']
        my_booking = Mydesk.query.filter_by(userid=id).all()

    return render_template('error.html', page_title=page_title, username=username, my_booking=my_booking)



@app.route('/remove')
def remove():
    if 'email' in session:
        id = session['id']
        email = session['email']
        
        Desknr.query.filter_by(booking_email=email).update(dict(booking_email='0', booking_code="0", booking_stat="0"))
        db.session.commit()

        Mydesk.query.filter_by(userid=id).delete()
        db.session.commit()

        return redirect(url_for('dashboard'))

@app.route('/error')
def error():
    page_title = {'name':'Book a Desk, ERROR...'}
    flash('wrong GET-Request')
    return render_template('404.html', page_title=page_title)


@app.route('/logout')
def logout():
    session.clear() 
    return redirect(url_for('login'))

if __name__ == '__main__':
    db.create_all()
    app.run(debug=True, host='0.0.0.0')
